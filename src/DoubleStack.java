//Näide: https://git.wut.ee/i231/home3/src/master/src/LongStack.java
//http://enos.itcollege.ee/~jpoial/algorithms/examples/Astack.java


import java.util.Iterator;
import java.util.LinkedList;
import java.util.StringTokenizer;

public class DoubleStack {
	private LinkedList<Double> stack; // muudetud privaatseks

	public static void main (String[] argum) {
      // TODO!!! Your tests here!
		double i = interpret("1 -2");
	      System.out.println(i);
   }

   DoubleStack() {
      // TODO!!! Your constructor here!
	   stack = new LinkedList<Double>();
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
	  DoubleStack newstack = new DoubleStack();
	  newstack.stack = (LinkedList<Double>) stack.clone();
      return newstack; // TODO!!! Your code here!
   }

   public boolean stEmpty() {
   // TODO!!! Your code here! Tagastab elementide arvu listis
      	if (stack.size() == 0) {
      		return true;
      	}
      	else {
      		return false;
      	}
   }

   public void push (double a) {
      // TODO!!! Your code here!
	   // Sisestab elemendi kõige algusesse
	   stack.push(a);
   }

   public double pop() {
	   //Eemaldab ning tagastab esimese elemendi
	   if (stack.isEmpty()) {
		   //viskab error'i ning annab teada, et indeks on tühi
		   throw  new  IndexOutOfBoundsException("Pole elemente");
		}
      return this.stack.pop(); // TODO!!! Your code here!
   } // pop

   public void op (String s) {
      // TODO!!!
	   //võrdleb string'i s konkreetse objektiga
	   if (stack.size()<2){
		   throw new RuntimeException(s+"pole piisavalt elemente.");
	   }
	   double b = stack.pop();
	   double a = stack.pop();
	   
	   if (s.equals ("+")) 
	   {
		   //System.out.println(b +" "+ s +" "+ a);
		   stack.push (b + a);
	   }
	   else if (s.equals ("-")) 
	   {
		   //System.out.println(a +" "+ s +" "+ b);
		   stack.push (a - b);
	   }
	   else if (s.equals ("*")) 
	   {
		   //System.out.println(b +" "+ s +" "+ a);
		   stack.push (b * a);
	   }
	   else if (s.equals ("/")) 
	   {
		   //System.out.println(a +" "+ s +" "+ b);
		   stack.push (a / b);
	   }
	   else
		   throw new RuntimeException("Selline tehe "+s+" ei ole lubatud!");
   }
  

public double tos() {
	if (stEmpty()) {
		throw new IndexOutOfBoundsException("Pole elemente antud operatsiooniks");
	}
      return this.stack.getFirst(); // TODO!!! Your code here!
   }

   @Override
   public boolean equals (Object o) {
      return this.stack.equals(((DoubleStack)o).stack); // TODO!!! Your code here!
   }

   @Override
   public String toString() {
      // TODO!!! Your code here!
	   StringBuilder b = new StringBuilder();
	   	Iterator<Double> i = this.stack.descendingIterator();
	   	while (i.hasNext()) {
	   		b.append(i.next());
	   		b.append(" ");
	   	}
	   	return b.toString();
   }

   public static double interpret (String pol) {
      // TODO!!! Your code here!
	   DoubleStack stack = new DoubleStack();

		// Split input string into array elements
		String[] elements = pol.trim().split("\\s+");

		int i = 0;
		int op = 0;
		
		// Enhanced for loop
		for (String element : elements) {
			try {
				stack.push(Double.valueOf(element));
				i++;
			} catch (NumberFormatException e) {
				if (!element.equals("+") && !element.equals("-")
						&& !element.equals("*") && !element.equals("/"))
					throw new RuntimeException(
							"Väljend sisaldab lubamatud sümbolit " + element+pol);
				else if (stack.stack.size() < 2)
					throw new RuntimeException("Väljend on liiga lühike!"+pol);
				stack.op(element);
				op++;
			}
		}
		if (i - 1 != op)
			throw new RuntimeException("Stack ei ole tasakaalus!"+pol);
		return stack.pop();
			 
   }

}

